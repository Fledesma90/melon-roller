using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceToTube : MonoBehaviour
{
    //ball gameobject
    public GameObject ball;
    //reference to ball2
    public GameObject ball2;
    //force applied to ball
    public float forceStrength;
    //timer to apply force
    public float forceToApplyTimer = 3f;
    //bool to not set collider
    private bool deactivateCol;
    //check if ball is in
    private bool ball1In;
    private bool ball2In;
    //can find ball2
    public bool canFind1 = true;
    //we can look for the other object to deactivate
    public bool canLookOtherObject;
    public GameObject [] objectToDeactivate;
    public float activationTimer;
    public float activationTime;
    public bool canCount;
    //references to particle systems
    public GameObject windRight;
    public GameObject windLeft;
    //todo wind sound when ball gets in touch with the wind
    // Start is called before the first frame update
    void Start()
    {
        //find ball
        ball = GameObject.FindWithTag("Ball");
        activationTime = activationTimer;
    }

    // Update is called once per frame
    void Update()
    {
        if (deactivateCol==true) {
            forceToApplyTimer -= Time.deltaTime;
            if (forceToApplyTimer<=0) {
                gameObject.GetComponent<BoxCollider>().enabled = true;
                deactivateCol = false;
                forceToApplyTimer = 3f;
            }
            
        }
        //if we haven't won the game and we have picked the powerup of X2 we stop looking for the ball2
        if (GameObject.Find("GameManager").GetComponent<GameManager>().hasWon == false && canFind1 == true) {
            if (GameObject.Find("GasExtraBall").GetComponent<Gas>().addBall == true) {
                ball2 = GameObject.FindWithTag("Ball2");
                canFind1 = false;
            }
        }
        if (canCount==true) {
            activationTimer -= Time.deltaTime;
            if (activationTimer<=0) {
                activationTimer = activationTime;
                canCount=false;
                foreach (GameObject objects in objectToDeactivate) {
                    objects.GetComponent<BoxCollider>().enabled = true;
                }
                windLeft.SetActive(true);
                windRight.SetActive(true);
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag=="Ball") {
            if (canLookOtherObject==true) {
                Debug.Log("entra bola1");
                foreach (GameObject objects in objectToDeactivate) {
                    objects.GetComponent<BoxCollider>().enabled = false;
                }
                ball.transform.rotation = transform.rotation;
                ball.transform.position = transform.position;
                windLeft.SetActive(false);
                windRight.SetActive(false);
                canCount = true;
            }
            ball1In = true;
            deactivateCol = true;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            if (ball1In==true) {
                ball.transform.rotation = transform.rotation;
                ball.transform.position = transform.position;
            }
            if (ball.transform.rotation==transform.rotation && ball.transform.position==transform.position) {
                ball.GetComponent<Rigidbody>().AddForce(transform.forward * forceStrength);
            }
          
            ball2In = false;
            deactivateCol = true;
            
        }
        if (other.gameObject.tag=="Ball2") {
            if (canLookOtherObject == true) {
                Debug.Log("entra bola2");
                foreach (GameObject objects in objectToDeactivate) {
                    objects.GetComponent<BoxCollider>().enabled = false;
                }
                ball2.transform.rotation = transform.rotation;
                ball2.transform.rotation = transform.rotation;
                windLeft.SetActive(false);
                windRight.SetActive(false);
                canCount = true;
            }
            gameObject.GetComponent<BoxCollider>().enabled = false;
            ball2In = true;
            ball1In = false;
            deactivateCol = true;
            if (ball2In==true) {
                ball2.transform.rotation = transform.rotation;
                ball2.transform.rotation = transform.rotation;
            }
            if (ball2.transform.rotation==transform.rotation && ball2.transform.position==transform.position) {
                ball.GetComponent<Rigidbody>().AddForce(transform.forward * forceStrength);
            }
        }
    }
}
