using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCameraCollider2 : MonoBehaviour
{
   
    //activa la camara de seguimiento en caso que una bola caiga
    public GameObject ball1Camera;
 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //private void OnCollisionEnter(Collision collision) {
    //    if (collision.gameObject.tag == "Ball2") {
    //        gameObject.GetComponent<BoxCollider>().enabled = false;
    //    }
    //}
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Ball2") {
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
