using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneController : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip buttonOverSound;
    public AudioClip insertCoinSound;
    public AudioClip playSound;
    public AudioClip hoverSound;
    public GameObject currentTab;
    public int currentSceneIndex;

    // Start is called before the first frame update
    void Start()
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeScene() {
        if (currentSceneIndex==0) {
            SceneManager.LoadScene(currentSceneIndex + 1);
        }
        if (currentSceneIndex == 1) {
            SceneManager.LoadScene(currentSceneIndex - 1);
        }
    }

    public void ChangeTab(GameObject nextScene) {
        currentTab.SetActive(false);
        currentTab = nextScene;
        currentTab.SetActive(true);
    }
    public void MainMenu() {
        SceneManager.LoadScene("MainMenu");
    }
    public void SetButtonOverSound() {
        audioSource.PlayOneShot(buttonOverSound);
    }

    public void InsertCoinSound() {
        audioSource.PlayOneShot(insertCoinSound);
    }

    public void PlayGameSound() {
        audioSource.PlayOneShot(playSound);
    }

    public void HoverSound() {
        audioSource.PlayOneShot(hoverSound);
    }

    public void ReloadScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void URLtransient() {
        Application.OpenURL("https://transientsignal.bandcamp.com/");
    }
}
