using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingScript : MonoBehaviour
{
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();
    public float frequency = 1f;
    public float amplitude = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        posOffset = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        tempPos = posOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        transform.position = tempPos;
    }
}
