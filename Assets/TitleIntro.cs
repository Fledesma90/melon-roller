using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleIntro : MonoBehaviour
{
    public GameObject title;
    // Start is called before the first frame update
    void Start()
    {
        title.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ActivateIntro (){
        title.SetActive(true);
    }
}
