using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TextAnim : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textMPRO;

    public string[] stringArray;
    public float timeBetweenCharacters;
    public float timeBetweenWords;
    int i = 0;
    // Start is called before the first frame update
    void Start()
    {
        EndCheck();
    }
    void EndCheck() {
        if (i<=stringArray.Length-1) {
            textMPRO.text = stringArray[i];
            StartCoroutine(TextVisible());
        }
    }
    private IEnumerator TextVisible() {
        textMPRO.ForceMeshUpdate();
        int totalVisibleCharacters = textMPRO.textInfo.characterCount;
        int counter = 0;

        while (true) {
            int visibleCount = counter % (totalVisibleCharacters + 1);
            textMPRO.maxVisibleCharacters = visibleCount;

            if (visibleCount>=totalVisibleCharacters) {
                i += 1;
                Invoke("EndCheck", timeBetweenWords);
                break;
            }

            counter += 1;
            yield return new WaitForSeconds(timeBetweenCharacters);
        }
    }
}
