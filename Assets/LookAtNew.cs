using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class LookAtNew : MonoBehaviour
{
    public bool canLookBall2;
    public GameObject ball2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (canLookBall2) {
            ball2 = GameObject.FindWithTag("Ball2");
            GetComponent<CinemachineVirtualCamera>().LookAt = ball2.transform;
            GetComponent<CinemachineVirtualCamera>().Follow = ball2.transform;
        }
    }
}
