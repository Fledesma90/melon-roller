using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelocateBall : MonoBehaviour
{
    public Transform balltransform;
    // Start is called before the first frame update
    void Start()
    {
        balltransform = GameObject.FindWithTag("Ball").transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other) {
        if (other.tag=="Ball") {
            other.transform.position = balltransform.position;
        }
    }
}
