using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour
{
    //reference to rb
    private Rigidbody rb;
    //speed the ball is going
    private int speed = 10;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {
        Controls();
    }
    /// <summary>
    /// Controls to move the ball
    /// </summary>
    private void Controls() {
        if (Input.GetKey(KeyCode.RightArrow)) {
            rb.AddForce(Vector3.right * speed);
        }
        if (Input.GetKey(KeyCode.LeftArrow)) {
            rb.AddForce(Vector3.left * speed);
        }
    }
}
