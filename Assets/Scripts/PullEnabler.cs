using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullEnabler : MonoBehaviour
{
    //reference to pullspring
    public PullSpring pullSpring;
    //counter to enable
    public float count=1f;
    public bool isPull;
    // Start is called before the first frame update
    void Start()
    {
        pullSpring=GetComponent<PullSpring>();
    }

    // Update is called once per frame
    void Update() {
        if (pullSpring.fire == false && pullSpring.ready == false) {
            pullSpring.enabled = false;
        }
        
    }
    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag=="Ball") {
            pullSpring.enabled = true;
        }
        if (collision.gameObject.tag == "Ball2") {
            pullSpring.enabled = true;
        }
    }
    /// <summary>
    /// if ball exits the pull's collider the pull script is deactivated
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionExit(Collision collision) {
        if (collision.gameObject.tag == "Ball") {
            pullSpring.enabled = false;
        }
    }
}
