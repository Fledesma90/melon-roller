using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassModifier : MonoBehaviour
{
    //bool with value to set from teleport collider
    public bool hasHit;
    //time to divide the ball rigidbody to
    public float massTimer=1f;
    //reference to rigidbody
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        //recover reference to rigidbody
        rb = GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //if ball has hit the teleport
        if (hasHit==true) {
            //the drag of the rigidbody is going to be 100 so the ball goes slow
            rb.drag = 100;
            //massTimer value is going to be add to time multiplied by a value to speed up the drag recovery
            //to drag default value
            massTimer += Time.deltaTime*40;
            //the drag is divided by the massTimer constantly
            rb.drag /= massTimer;
            //when the drag is below or equal to 1
            if (rb.drag<=1f) {
                //the drag is going to be 0 as it was before hitting the collider
                rb.drag = 0f;
                //establish massTimer to its default value
                massTimer = 1f;
                //keep hasHit in its default value
                hasHit = false;

            }
        }
    }
}
