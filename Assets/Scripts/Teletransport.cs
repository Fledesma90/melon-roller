using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teletransport : MonoBehaviour
{
    //position where the ball is going to be teleported
    public Transform telePosition;
    //ball gameobject
    public GameObject ball;
    //balltransform
    public Vector3 ballTrans;
    //bool to let ball teleport
    public bool canTrans;
    //time in which teleport is available again
    public float timeToTeletransport=4f;
    //bool to set timer to make teleport available again after balls collide with teleport
    public bool isTime;
    //reference to audiosource
    private AudioSource audioSource;
    //reference to sounds
    public AudioClip sound;
    //public GameObject ps;
    public ParticleSystem ps;
    public ParticleSystem ps1;
    //reference to ball2
    public GameObject ball2;
    //can find ball2
    public bool canFind1 = true;
    //check which ball is within
    public bool ball2In;
    public bool ball1In;
    // Start is called before the first frame update
    void Start()
    {
        //ps.SetActive(false);
        audioSource = GetComponent<AudioSource>();
        //find ball
        ball = GameObject.FindWithTag("Ball");
        //position where the ball is going to teleport
        telePosition = GameObject.FindWithTag("Transfer").transform;
    }

    // Update is called once per frame
    void Update()
    {
        //verify if timer is true and if ball2 is false
        if (isTime==true&&ball2In==false) {
            //change the ball mass modifier to true so mass changes and speed gets slower when teleported
            ball.GetComponent<MassModifier>().hasHit = true;
            //disable box collider so ball doesn't keep teleporting time after time
            gameObject.GetComponent<BoxCollider>().enabled = false;
            //if ball gets in touch with collider, its position changes to the collider of teleport
            ball.transform.position = transform.position;
            //ensure ball's position is teleport's position by constantly checking it out
            if (ball.transform.position!=transform.position) {
                ball.transform.position = transform.position;
            }
            //start teleport counter
            timeToTeletransport -= Time.deltaTime;
            //reset teleport's values after timer is over
            if (timeToTeletransport<=0) {
                gameObject.GetComponent<BoxCollider>().enabled = true;
                ball.transform.position = telePosition.transform.position;
                timeToTeletransport = 4f;
                isTime = false;
            }
        }
        //same checks with ball2
        if (isTime == true && ball2In == true) {
            ball2.GetComponent<MassModifier>().hasHit = true;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            ball2.transform.position = transform.position;
            if (ball2.transform.position != transform.position) {
                ball2.transform.position = transform.position;
            }
            timeToTeletransport -= Time.deltaTime;
            if (timeToTeletransport <= 0) {
                gameObject.GetComponent<BoxCollider>().enabled = true;
                ball2.transform.position = telePosition.transform.position;
                timeToTeletransport = 4f;
                ball2In = false;
                isTime = false;
            }
        }
        //if we haven't won the game and we have picked the powerup of X2 we stop looking for the ball2
        if (GameObject.Find("GameManager").GetComponent<GameManager>().hasWon==false && canFind1 == true) {
            if (GameObject.Find("GasExtraBall").GetComponent<Gas>().addBall == true) {
                ball2 = GameObject.FindWithTag("Ball2");
                canFind1 = false;
            }
        }
    }

    //actions after ball got collision with teleport
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag=="Ball") {
            GameObject.Find("GameManager").GetComponent<GameManager>().scoreInt += 20;
            ball2In = false;
            ps.Play();
            ps1.Play();
            isTime = true;
            audioSource.PlayOneShot(sound);
        }
        if (other.gameObject.tag == "Ball2") {
            GameObject.Find("GameManager").GetComponent<GameManager>().scoreInt += 20;
            ball2In = true;
            ball1In = false;
            ps.Play();
            ps1.Play();
            isTime = true;
            audioSource.PlayOneShot(sound);
        }
    }
}
