using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelTeletransport : MonoBehaviour
{
    //reference to teleport
    public GameObject teletransport;

    // Start is called before the first frame update
    void Start()
    {
        //find the teleport
        teletransport = GameObject.FindWithTag("Teletransport");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag=="Ball") {
            //disable script
            teletransport.GetComponent<Teletransport>().canTrans = false;
            //start coroutine to disable and enable again after teleporting has been done
            StartCoroutine(EnableTeletransport());
        }
    }

    IEnumerator EnableTeletransport() {
        teletransport.GetComponent<Teletransport>().enabled = false;
        yield return new WaitForSeconds(5);
        teletransport.GetComponent<Teletransport>().enabled = true;
    }
}
