using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningBehaviour : MonoBehaviour
{
    public Transform asteroidTrans;
    public GameObject asteroid;
    private AudioSource audioSource;
    public AudioClip hitWarningSound;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Ball") {
            Instantiate(asteroid, asteroidTrans.position, Quaternion.identity);
            StartCoroutine(CollisionDisabler());
        }
        if (other.gameObject.tag == "Ball2") {
            Instantiate(asteroid, asteroidTrans.position, Quaternion.identity);
            StartCoroutine(CollisionDisabler());
        }
    }
    IEnumerator CollisionDisabler() {
        audioSource.PlayOneShot(hitWarningSound);
        GetComponent<BoxCollider>().enabled = false;
        yield return new WaitForSeconds(5);
        GetComponent<BoxCollider>().enabled = true;
        
    }
}
