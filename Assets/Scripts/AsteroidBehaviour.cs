using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBehaviour : MonoBehaviour
{
    //Pieces of the asteroid
    public GameObject asteroidPieces;
    //Transform array in which pieces of asteroids are going to be instantiated
    public Transform[] asteroidPiecesTransform;
    //reference to particle system of the explosion
    public GameObject ps;
    //audiosource component reference
    private AudioSource audioSource;
    //sound of impact
    public AudioClip sound;
    // Start is called before the first frame update
    void Start()
    {
        //get rb component and disable gravity
        GetComponent<Rigidbody>().useGravity = false;
        //get audiosource reference
        audioSource=GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //gameobject falls down by translating it's movement downwards
        transform.Translate(0, -0.3f, 0);
    }

    private void OnCollisionEnter(Collision collision) {
        //if asteroids gets in contact with the floor
        if (collision.gameObject.GetComponent<Floor>()) {
            //instantiate asteroid pieces on their transforms
            foreach (Transform item in asteroidPiecesTransform) {
                Instantiate(asteroidPieces, gameObject.transform.position, Quaternion.identity);
            }
            //add score to score count
            GameObject.Find("GameManager").GetComponent<GameManager>().scoreInt += 5;
            //play sound
            audioSource.PlayOneShot(sound);
            //disable its collider
            gameObject.GetComponent<SphereCollider>().enabled = false;
            //disable its mesh
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            //invoke method
            Invoke("Destruction", 2f);
        }
    }
    /// <summary>
    /// destroy gameobject
    /// </summary>
    void Destruction() {
        Destroy(gameObject);
    }
}
