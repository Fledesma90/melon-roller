using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipperScript : MonoBehaviour
{
    //position of flipper in resting mode
    public float restPosition = 0f;
    //position when pressed
    public float pressedPosition = 45f;
    //strenght
    public float hitStrenght = 100000f;

    public float flipperDamper = 150f;
    //reference to component hinge
    HingeJoint hinge;
    //audiosource
    private AudioSource audioSource;
    //audioclip
    public AudioClip audioFlipper;
    //flippers' sound won't play until this bool is true
    private bool canPlay=true;
    //counter until next time of sound to be player can be possible
    private float playCounter = 2f;
    //counter enabler
    private bool canCount;
    // Start is called before the first frame update
    void Start()
    {
        //catch audiosource reference
        audioSource = GetComponent<AudioSource>();
        //catch hingejoint component
        hinge = GetComponent<HingeJoint>();
        //enable possibilite of usespring
        hinge.useSpring = true;
    }

    // Update is called once per frame
    void Update() {
        //reference to JointSprint and we call it spring
        JointSpring spring = new JointSpring();
        //embed spring with hitstrenght value
        spring.spring = hitStrenght;
        //add spring the value of flipperdamper
        spring.damper = flipperDamper;
        //we play sound if conditions are met
        if (Input.GetMouseButton(0)) {
            if (canPlay == true) {
                audioSource.PlayOneShot(audioFlipper);
                canCount = true;
            }
            spring.targetPosition = pressedPosition;
        } else {
            spring.targetPosition = restPosition;
        }
        hinge.spring = spring;
        hinge.useLimits = true;
        CountForSound();
    }
    /// <summary>
    /// Cunter to play sound
    /// </summary>
    private void CountForSound() {
        if (canCount == true) {
            playCounter -= Time.deltaTime;
        }

        if (playCounter < 2) {
            canPlay = false;
        }
        if (playCounter <= 0) {
            canCount = false;
            canPlay = true;
            playCounter = 2;
        }
    }
}
