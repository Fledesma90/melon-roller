using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncer : MonoBehaviour
{
    //reference to the ball
    public GameObject ball;
    //force to apply by bouncer
    public float bouncerForce=500f;
    //rigidbody of the ball
    public Rigidbody ballrb;
    //time that allows the bouncer to increment its force if ball hit bouncer again before bounceTimer ends
    public float bounceTimer = 2f;
    //force multiplier 
    public float bouncerMultiplier=1f;
    //set timer
    public bool timerToBounce;
    //condition for multiplier to be working
    public bool timerToBounceMultiplier;
    //number until the counter can count when subsctracting time.deltatime
    public float multiplierCounter=6f;
    //ball2
    public GameObject ball2;
    //can
    public bool canFind=true;

    private AudioSource audioSource;
    public AudioClip hitSound;
    public ParticleSystem ps;
    // Start is called before the first frame update
    void Start()
    {

        audioSource = GetComponent<AudioSource>();
        ball = GameObject.FindWithTag("Ball");
        ballrb= ball.GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        //if powerup has been picked and condition of winning or losing is false we can search for the ball2
        if (canFind==true && GameObject.Find("GameManager").GetComponent<GameManager>().hasWon==false && GameObject.Find("GameManager").GetComponent<GameManager>().hasLost == false) {
            if (GameObject.Find("GasExtraBall").GetComponent<Gas>().addBall == true) {
                ball2 = GameObject.FindWithTag("Ball2");
                canFind = false;
            }
        }
        
        
      
        if (timerToBounce==true) {
            bounceTimer -= Time.deltaTime;
           
            if (bounceTimer<=0) {
                timerToBounce = false;
                bounceTimer = 2f;
            }
        }
        if (timerToBounceMultiplier==true) {
            multiplierCounter -= Time.deltaTime;
            if (multiplierCounter<=0) {
                timerToBounceMultiplier = false;
                multiplierCounter = 6f;
                bouncerMultiplier = 1f;
            }
        }
        if (bouncerMultiplier==5f) {
            bouncerMultiplier = 1f;
        }


    }
    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag=="Ball") {
            //play the sound
            audioSource.PlayOneShot(hitSound);
            //apply a force to the ball
            ballrb.AddExplosionForce(bouncerForce*bouncerMultiplier, this.transform.position, 5);
            //add the multiplier a value
            bouncerMultiplier += 0.5f;
            //set timer since last bounce
            timerToBounce = true;
            //we can start counting until bounceMultiplier is going to be the new value after adding 0.5 or until it comes back to 1
            timerToBounceMultiplier = true;
            //add score
            GameObject.Find("GameManager").GetComponent<GameManager>().scoreInt += 10;
            //playrays particle system
            ps.Play();
        }

        if (collision.gameObject.tag=="Ball2") {
            audioSource.PlayOneShot(hitSound);
            collision.gameObject.GetComponent<Rigidbody>().
                AddExplosionForce(bouncerForce * bouncerMultiplier, this.transform.position, 5);
            bouncerMultiplier += 0.5f;
            timerToBounce = true;
            timerToBounceMultiplier = true;
            GameObject.Find("GameManager").GetComponent<GameManager>().scoreInt += 10;
            //playrays particle system
            ps.Play();
        }
    }
}
