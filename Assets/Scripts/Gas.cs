using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class Gas : MonoBehaviour
{
    //reference to the ball
    public GameObject ball;
    //transform of the ball
    public Transform ballTrans;
    //ps that is going to be instantiated after ball collides with powerup
    public GameObject ps;
    //reference to audiosource
    private AudioSource audioSource;
    //reference to audioclip
    public AudioClip sound;
    //reference to camcollider to change camera
    public GameObject camCollider;
    //reference to the trigger object that is going to hit the camCollider to change camera
    public GameObject triggerBall;
    //add a ball to all powerups
    public bool addBall;
    // Start is called before the first frame update
    public GameObject floor;
    //reference to the model
    public GameObject model;
    void Start()
    {
        //get reference to audiosource
        audioSource = GetComponent<AudioSource>();
        //get reference of camCollider
        camCollider = GameObject.Find("CameraTrigger");
        //disable camCollider and we'll enable it later when Ball hits powerup so the
        //camera changes to see a full view of the pinball board
        camCollider.GetComponent<BoxCollider>().enabled = false;
        floor = GameObject.Find("FloorCollider");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag=="Ball") {
            GameObject.Find("GameManager").GetComponent<GameManager>().scoreInt += 25;
            //set bool to true in gamemanager so it can look for secondball if available and destroy it
            GameObject.Find("GameManager").GetComponent<GameManager>().isSecondBall = true;
            floor.GetComponent<FloorLimit>().lives++;
            model.SetActive(false);
            addBall = true;
            triggerBall.SetActive(true);
            Instantiate(ps, gameObject.transform.position, Quaternion.identity);
            Instantiate(ball, ballTrans.position, Quaternion.identity);
            camCollider.GetComponent<BoxCollider>().enabled = true;
            audioSource.PlayOneShot(sound);
            gameObject.GetComponent<BoxCollider>().enabled = false;
            Invoke("DestructionFX", 2f);
        }
    }
    /// <summary>
    /// Destroy gameObject
    /// </summary>
    void DestructionFX() {
        addBall = false;
        Destroy(gameObject);
    }
}
