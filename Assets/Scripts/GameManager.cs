using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class GameManager : MonoBehaviour {
    //float of counter of distance
    public float counterFloat;
    //int of counter of distance
    public int counterInt;
    //distance to destination
    public int numberToWin = 200;
    //reference to text element in which count is going to appear on UI
    public TextMeshProUGUI text;
    //image of victory
    public GameObject winImage;
    //image of defeat
    public GameObject loseImage;
    //particle system of victory
    public GameObject winPS;
    //bools that is going to be used to cancel search for balls on the rest of scripts
    public bool hasWon;
    public bool hasLost;
    //reference to death floor
    private GameObject floorCollider;
    //condition that enables the counter
    public bool canCount;
    //reseteamos el nivel
    public bool reset;
    //reset counter to reset level
    public bool resetCount;
    //int to reset level
    public float resetInt;
    //dialogue of Melody
    public GameObject dialogueImage;
    //audiosource
    private AudioSource audioSource;
    //sound
    public AudioClip astronautSound;
    //reference to pull
    public GameObject pull;
    //references to flippers
    public GameObject rflipper;
    public GameObject lflipper;
    //collider de la primera cinematica
    public GameObject TriggerCol1;
    //box that is going to stop the ball until first dialogue has finished
    public GameObject boxStopper;
    //astronaut gameobjects
    public GameObject waveAstronaut;
    public GameObject defeatAstronautModel;
    public GameObject triggerColDieCine;
    //bool to establish that you can only lose one time in the update and not keep losing time after time every frame
    private bool justOneLose;
    //number to compare with time delta time the condition of lose
    private float canLose = 2;
    //reference to winning astronaut
    public GameObject winAstronaut;
    //condition to put only one win event
    private bool justOneWin;
    //number to compare if it can win
    private float canWin = 99f;
    //int to show the score
    public int scoreInt;
    //score UI
    public TextMeshProUGUI scoreUI;
    //reference to second ball
    public GameObject ball2;
    //verify if it is a secondball
    public bool isSecondBall;
    //bool to tell when to increase animation
    private bool animationTextDistanceIncrease;
    //bool to tell when to decrease animation
    private bool animationTextDistanceDecrease;
    //bool to tell if it can animate
    private bool canAnim = true;
    // Start is called before the first frame update
    public ParticleSystem winPSCounter;
    //TODO Sound of winning sparkles
    //todo sound of fireworks
    private Vector3 scalePSChange;

    private GameObject lookAtNew;
    void Start() {
        lookAtNew = FindObjectOfType<LookAtNew>().gameObject;
        //pick reference to audiosource
        audioSource = GetComponent<AudioSource>();
        //pick reference to floor
        floorCollider = GameObject.Find("FloorCollider");
        //invoke cinematics
        Invoke("IsCinematic1", 4f);
        //there's no second ball at the beginning of game
        isSecondBall = false;
    }

    // Update is called once per frame
    void Update() {

        Counter();
        WinGame();
        Score();
        //conditions to lose game and destroy balls
        if (floorCollider.GetComponent<FloorLimit>().lives == 0 && justOneLose == false && justOneWin == false) {
            justOneLose = true;
            LoseGame();
            SearchBalls();
            //set the ballcamera2 to false so it doesn't look for the ball2
            lookAtNew.GetComponent<LookAtNew>().canLookBall2 = false;
        }
        //if justOneLose is true
        if (justOneLose == true) {
            //carry out canLoseCounter
            canLose += Time.deltaTime;
        }
        //canLose counter is different from 2
        if (canLose != 2) {
            //justOneLose is false
            justOneLose = false;
            //and we set lives to -1
            floorCollider.GetComponent<FloorLimit>().lives = -1;
            //putting just one lose into true again
            justOneLose = true;
        }
        Controls();
        if (resetInt >= 6) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }
        //if the counter is equal to the number to win
        if (counterInt == numberToWin) {
            //we add canWin float the time
            canWin += Time.deltaTime;
        }
        //if canWin is different from 99
        if (canWin != 99) {
            //we set justOneWin to true so the method of winning doesn't loop
            justOneWin = true;
        }

        TextAnimation();
    }
    /// <summary>
    /// Distance text shows a little animation when winning the game
    /// </summary>
    private void TextAnimation() {
        //we do animation of distance text
        if (counterInt == numberToWin && animationTextDistanceIncrease == false && canAnim == true) {
            animationTextDistanceIncrease = true;
            animationTextDistanceDecrease = false;
            text.color = new Color32(219, 184, 34, 255);
            winPSCounter.Play();
        }
        if (counterInt == numberToWin && text.fontSize >= 65.5 && animationTextDistanceIncrease == true) {
            animationTextDistanceIncrease = false;
            animationTextDistanceDecrease = true;
        }
        if (animationTextDistanceIncrease == true && animationTextDistanceDecrease == false) {
            text.fontSize += Time.deltaTime * 35;
            if (text.fontSize >= 65.5) {
                animationTextDistanceIncrease = false;
                animationTextDistanceDecrease = true;
                canAnim = false;
                text.fontSize -= Time.deltaTime * 35;
            }
        }
        if (animationTextDistanceIncrease == false && animationTextDistanceDecrease == true) {
            text.fontSize -= Time.deltaTime * 35;
            if (text.fontSize <= 45.5) {
                animationTextDistanceIncrease = false;
                animationTextDistanceDecrease = false;
                canAnim = false;
                text.fontSize = 45.5f;
            }
        }
    }
    /// <summary>
    /// Distance counter
    /// </summary>
    private void Counter() {
        if (canCount == true) {
            counterFloat += 1 * Time.deltaTime;
            counterInt = Mathf.RoundToInt(counterFloat);
            text.text = counterInt.ToString();
        }
    }
    /// <summary>
    /// Show score on UI
    /// </summary>
    public void Score() {
        scoreUI.text = scoreInt.ToString();
    }
    /// <summary>
    /// Events and conditions to every step of winning the game
    /// </summary>
    void WinGame() {
        //if the counter meets the distance to win and the event of winning hasn't happened yet
        if (counterInt == numberToWin && justOneWin == false) {
            hasWon = true;
            canCount = false;
            winImage.SetActive(true);
            winAstronaut.SetActive(true);
            waveAstronaut.SetActive(false);
            DisableMovements1();
            triggerColDieCine.SetActive(true);
            //set the ballcamera2 to false so it doesn't look for the ball2
            lookAtNew.GetComponent<LookAtNew>().canLookBall2 = false;
            SearchBalls();
        }
        //if winning has happened then do this
        if (justOneWin == true) {
            winImage.SetActive(true);
            winAstronaut.SetActive(true);
            waveAstronaut.SetActive(false);
            winPS.SetActive(true);
            pull.GetComponent<PullSpring>().enabled = false;
            pull.GetComponent<PullEnabler>().enabled = false;
            rflipper.GetComponent<FlipperScript>().enabled = false;
            lflipper.GetComponent<FlipperScript>().enabled = false;
        }
        PSscale();
    }
    /// <summary>
    /// Look and destroy balls
    /// </summary>
    private void SearchBalls() {
        if (isSecondBall == true) {
            ball2 = GameObject.FindWithTag("Ball2");
            lookAtNew.GetComponent<LookAtNew>().canLookBall2 = false;
            Destroy(ball2);
        }
        GameObject ball;
        ball = GameObject.FindWithTag("Ball");
        Destroy(ball);
    }
    /// <summary>
    /// Events that are going to happen when losing the game
    /// </summary>
    void LoseGame() {
        hasLost = true;
        canCount = false;
        loseImage.SetActive(true);
        defeatAstronautModel.SetActive(true);
        waveAstronaut.SetActive(false);
        triggerColDieCine.SetActive(true);
        DisableMovementsDie();
    }
    /// <summary>
    /// controls to reset game
    /// </summary>
    void Controls() {
        if (Input.GetMouseButton(0)) {
            resetCount = true;
        } else {
            resetCount = false;
            resetInt = 0f;
        }
        if (resetCount == true) {
            resetInt += Time.deltaTime;
        }
    }
    /// <summary>
    /// disable movements of board
    /// </summary>
    void DisableMovements1() {
        pull.GetComponent<PullSpring>().enabled = false;
        pull.GetComponent<PullEnabler>().enabled = false;
        rflipper.GetComponent<FlipperScript>().enabled = false;
        lflipper.GetComponent<FlipperScript>().enabled = false;
        //canCount = false;
        audioSource.PlayOneShot(astronautSound);
    }
    /// <summary>
    /// enable movements of board
    /// </summary>
    void EnableMovements() {
        pull.GetComponent<PullSpring>().enabled = true;
        pull.GetComponent<PullEnabler>().enabled = true;
        rflipper.GetComponent<FlipperScript>().enabled = true;
        lflipper.GetComponent<FlipperScript>().enabled = true;
        //canCount = true;
        TriggerCol1.SetActive(true);
        boxStopper.SetActive(false);
    }
    /// <summary>
    /// cinematics
    /// </summary>
    /// <returns></returns>
    IEnumerator Cinematic() {
        DisableMovements1();
        dialogueImage.SetActive(true);
        yield return new WaitForSeconds(10);
        dialogueImage.SetActive(false);
        EnableMovements();
    }
    /// <summary>
    /// method of coroutine of cinematics
    /// </summary>
    void IsCinematic1() {
        StartCoroutine(Cinematic());
    }
    /// <summary>
    /// Method to disable movements
    /// </summary>
    void DisableMovementsDie() {
        pull.GetComponent<PullSpring>().enabled = false;
        pull.GetComponent<PullEnabler>().enabled = false;
        rflipper.GetComponent<FlipperScript>().enabled = false;
        lflipper.GetComponent<FlipperScript>().enabled = false;
        canCount = false;
        audioSource.PlayOneShot(astronautSound);
    }
    /// <summary>
    /// SuperNovaFX by decreasing particlesystem scale
    /// </summary>
    void PSscale() {
        Vector3 topScale;
        topScale = new Vector3(-3, -3, -3);
        scalePSChange = new Vector3(0.1f, 0.1f, 0.1f);
        if (winPSCounter.transform.localScale.magnitude <= topScale.magnitude&&
            canCount == false && hasWon == true) {
            winPSCounter.transform.localScale -= scalePSChange;
        }
    }
}
