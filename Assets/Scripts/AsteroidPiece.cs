using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidPiece : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //destroy asteroid piece
        Destroy(gameObject, 1.5f);
    }
}
