using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class TriggerCameraCollider : MonoBehaviour
{
    //activa la camara de seguimiento en caso que una bola caiga
    public GameObject CameraBall1Active;
    public GameObject CameraBall2Active;
    // Start is called before the first frame update
    void Start()
    {
        CameraBall1Active.GetComponent<BoxCollider>().enabled = false;
        CameraBall2Active.GetComponent<BoxCollider>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //private void OnCollisionEnter(Collision collision) {
    //    if (collision.gameObject.tag=="Ball") {
    //        gameObject.GetComponent<BoxCollider>().enabled = false;
    //        CameraBall1Active.GetComponent<BoxCollider>().enabled = true;
    //        CameraBall2Active.GetComponent<BoxCollider>().enabled = true;
    //    }
    //}
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag=="Ball") {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            CameraBall1Active.GetComponent<BoxCollider>().enabled = true;
            CameraBall2Active.GetComponent<BoxCollider>().enabled = true;
        }
    }

}
