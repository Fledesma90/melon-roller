using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullSpring : MonoBehaviour
{
    //maximum distance the pull is going to reach
    public float distance = 50f;
    //Speed in which the pull is going down
    public float speed = 1f;
    //the object we are going to catapult
    public GameObject ball;
    //power of pull
    public float power=2000f;
    //ready to pull
    public bool ready=false;
    //release
    public bool fire = false;

    public float moveCount = 0f;
    //reference to rigidbody
    public Rigidbody rb;
    //condition of pull being able to move
    public bool canTranslate;
    //reference to audiosource
    private AudioSource audioSource;
    //audios
    public AudioClip pullClip;
    public AudioClip releaseClip;
    public AudioClip ballHitPull;
    //variables para hacer el sonido de contacto
    public bool canMakeSound=true;
    private float playCounter = 3f;
    private bool canCount;
    //variables para hacer el sonido de liberación de la bola
    public bool canPlayPull;
    //counter to play sound just once every certain time
    private float playSFXcounter=3f;
    //condition for sound counter
    private bool canCountSFXP;

    // Start is called before the first frame update
    void Start()
    {
        //when starting the game the pull can make the sounds
        canMakeSound = true;
        canPlayPull = true;
        //get reference of audiosource
        audioSource = GetComponent<AudioSource>();
        //find the ball
        ball = GameObject.FindWithTag("Ball");
        //get ball's rigidbody
        rb = ball.GetComponent<Rigidbody>();
    }
    
    // Update is called once per frame
    void Update() {
        PlayReleaseSound();
        if (Input.GetMouseButton(0)) {
            if (moveCount < distance) {
                if (canPlayPull == true) {
                    audioSource.PlayOneShot(pullClip);
                    canCountSFXP = true;
                }
                transform.Translate(0, 0, -speed * Time.deltaTime);
                moveCount += speed * Time.deltaTime;
                fire = true;
            }
        } else if (moveCount > 0) {
            //Shoot the ball
            if (fire && ready) {
                audioSource.PlayOneShot(releaseClip);
                ball.transform.TransformDirection(Vector3.forward * 10);
                rb.AddForce(0, 0, moveCount * power);
                fire = false;
                ready = false;
            }
            //Once we have reached the starting position fire off!
            transform.Translate(0, 0, 20 * Time.deltaTime);
            moveCount -= 20 * Time.deltaTime;
        }

        //Just ensure we don't go past the end
        if (moveCount <= 0) {
            fire = false;
            moveCount = 0;
        }

        PlayCollisionSound();
    }
    /// <summary>
    /// play the sound of release pull
    /// </summary>
    private void PlayReleaseSound() {
        if (canCountSFXP == true) {
            playSFXcounter -= Time.deltaTime;
        }
        if (playSFXcounter < 3) {
            canPlayPull = false;
        }
        if (playSFXcounter <= 0) {
            canCountSFXP = false;
            canPlayPull = true;
            playSFXcounter = 3f;
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag=="Ball") {
            ready = true;
            if (canMakeSound==true) {
                audioSource.PlayOneShot(ballHitPull);
                canCount = true;
            }
        } 
    }
    /// <summary>
    /// Play the sound of collision when ball collides with pull
    /// </summary>
    void PlayCollisionSound() {
        if (canCount==true) {
            playCounter -= Time.deltaTime;
        }
        if (playCounter<3) {
            canMakeSound = false;
        }
        if (playCounter<=0) {
            canCount = false;
            canMakeSound = true;
            playCounter = 3;
        }
    }

}
