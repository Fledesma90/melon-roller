using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorEnabler : MonoBehaviour
{
    //asteroid that is going to be placed after ball hits the end of
    //pull path
    public GameObject asteroid;
    private AudioSource audioSource;
    public AudioClip asteroidClip;
    public GameObject gameM;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        asteroid.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag=="Ball") {
            //we can start counting the distance
            gameM.GetComponent<GameManager>().canCount = true;
            //play sound of asteroid falling
            audioSource.PlayOneShot(asteroidClip);
            //enable asteroid
            asteroid.SetActive(true);
            //disable collider
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
